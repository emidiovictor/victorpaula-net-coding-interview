﻿using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Core.Services;

namespace SecureFlight.Api.Services;

public class PassengerService(IRepository<Passenger> repository, IRepository<Flight> flightRepository)
    : BaseService<Passenger>(repository), IPassengerService
{
    public async Task<OperationResult<Passenger>> AddPassengerOnFlight(long code, string passengerCode)
    {
        var flight = await flightRepository.GetByIdAsync(code);
        var passenger = await repository.GetByIdAsync(passengerCode);
        flight.AddPassenger(passenger);
        await flightRepository.SaveChangesAsync();
        return passenger;
    }
}