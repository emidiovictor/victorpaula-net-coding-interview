﻿using SecureFlight.Api.Models;
using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Core.Services;

namespace SecureFlight.Application.Services;

public class AirportService(IRepository<Airport> repository) : BaseService<Airport>(repository), IAirPortService
{
    public async Task<OperationResult<Airport>> UpdateAsync(AirportDataTransferObject updatedAirport)
    {
        var airPortResult = await FindAsync(updatedAirport.Code);
        if (!airPortResult.Succeeded)
        {
            return airPortResult;
        }

        var airport = airPortResult.Result;

        airport.City = updatedAirport.City;
        airport.Country = updatedAirport.Country;
        airport.Name = updatedAirport.Name;
        repository.Update(airport);
        await repository.SaveChangesAsync();
        return airPortResult;
    }
}