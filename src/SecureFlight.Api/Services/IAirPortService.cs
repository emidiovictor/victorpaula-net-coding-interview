﻿using SecureFlight.Api.Models;
using SecureFlight.Core;
using SecureFlight.Core.Entities;

namespace SecureFlight.Application.Services;

public interface IAirPortService
{
    Task<OperationResult<Airport>> UpdateAsync(AirportDataTransferObject updatedAirport);
}