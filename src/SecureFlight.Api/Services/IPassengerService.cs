﻿using SecureFlight.Core;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Services;

public interface IPassengerService : IService<Passenger>
{
    Task<OperationResult<Passenger>> AddPassengerOnFlight(long code, string passengerCode);
}