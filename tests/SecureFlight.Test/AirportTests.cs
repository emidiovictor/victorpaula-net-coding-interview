using System.Threading.Tasks;
using SecureFlight.Api.Models;
using SecureFlight.Application.Services;
using SecureFlight.Core.Entities;
using SecureFlight.Infrastructure.Repositories;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public async Task Update_Succeeds()
        {
            //Arrange
            var testingContext = new SecureFlightDatabaseTestContext();
            testingContext.CreateDatabase();
            var repository = new BaseRepository<Airport>(testingContext);
            var sut = new AirportService(repository);
            //Act

            var code = "AAQ";

            var airport = await repository.GetByIdAsync(code);
            var name = airport.Name;
            var city = airport.City;
            var country = airport.Country;

            var dto = new AirportDataTransferObject(code, name + "1", city + "1", country + "1");

            var updatedAirPortResult = await sut.UpdateAsync(dto);


            var updatedAirPort = updatedAirPortResult.Result;


            //Assert
            Assert.NotSame(name, updatedAirPort.Name);
            Assert.NotSame(city, updatedAirPort.City);
            Assert.NotSame(country, updatedAirPort.Country);

            testingContext.DisposeDatabase();
        }
    }
}